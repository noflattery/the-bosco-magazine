<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

	<head>
		<meta charset="utf-8">

		<?php // force Internet Explorer to use the latest rendering engine available ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title><?php wp_title(''); ?></title>

		<?php // mobile meta (hooray!) ?>
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1"/>

		<?php // icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) ?>
		<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-touch-icon.png">
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<?php // or, set /favicon.ico for IE10 win ?>
		<meta name="msapplication-TileColor" content="#f01d4f">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">
        <meta property="og:title" content="<?php the_title(); ?>" />
		<meta property="og:image" content="<?php the_post_thumbnail_url(); ?>" />
		<meta property="og:image:type" content="image/jpeg" />

            <meta name="theme-color" content="#121212">

        <!-- bootstrap styles -->
        <link href="<?php echo get_template_directory_uri(); ?>/library/css/bootstrap.min.css" rel="stylesheet">

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<?php // wordpress head functions ?>

		<?php wp_head(); ?>
		<?php if (is_single()) : ?>
			<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/library/js/libs/featherlight.js"></script>
			<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/library/js/libs/featherlightgallery.js"></script>
			<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/library/js/libs/isInViewport.min.js"></script>	
			<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/library/js/libs/setLocationHash.min.js"></script>			
			<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/library/js/libs/smooth-scroll.min.js"></script>
		<?php endif; ?>
		<?php // end of wordpress head ?>

		<?php // drop Google Analytics Here ?>
		<script type="text/javascript">
		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-26808254-1']);
		  _gaq.push(['_setDomainName', 'magazine.thebosco.com']);
		  _gaq.push(['_setAllowLinker', true]);
		  _gaq.push(['_trackPageview']);

		  (function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();

		</script>
		<?php // end analytics ?>

	</head>

	<body <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage">

		

			<header class="header" role="banner" itemscope itemtype="http://schema.org/WPHeader">

				<div id="inner-header" class="cf">

					<div class="col-xs-4 col-md-4 header-logo">
						<a href="<?php echo home_url(); ?>" rel="nofollow">
							<?php if (is_page_template( 'page-archive.php' ) || is_search()) : ?>

								<img src="<?php echo get_template_directory_uri(); ?>/library/images/boscomag-vector-black.svg" class="company-logo" />
								<?php elseif (is_archive()) : ?>
									<img src="<?php echo get_template_directory_uri(); ?>/library/images/boscomag-vector-black.svg" class="company-logo" />
								<?php else :?>
								<img src="<?php echo get_template_directory_uri(); ?>/library/images/boscomag-vector-white.svg" class="company-logo" />

							<?php endif; ?>
						</a>
						<?php // if you'd like to use the site description you can un-comment it below ?>
						<?php // bloginfo('description'); ?>
					</div>

					<div class="col-xs-4 col-md-4 menu-toggle">
						<a class="btn-open" href="#">
							<?php if (is_page_template( 'page-archive.php' ) || is_search()) : ?>
								<img src="<?php echo get_template_directory_uri(); ?>/library/images/hamburger-black.svg" />
								<?php elseif (is_archive()) : ?>
									<img src="<?php echo get_template_directory_uri(); ?>/library/images/hamburger-black.svg" />
								<?php else :?>
								<img src="<?php echo get_template_directory_uri(); ?>/library/images/hamburger-white.svg" />
							<?php endif; ?>
						</a>
					</div>

					<div class="col-xs-4 col-md-4 search-button">
						<a class="btn-open" href="#">
							<?php if (is_page_template( 'page-archive.php' ) || is_search()) : ?>
								<img src="<?php echo get_template_directory_uri(); ?>/library/images/search-icon-black.svg" />
								<?php elseif (is_archive()) : ?>
									<img src="<?php echo get_template_directory_uri(); ?>/library/images/search-icon-black.svg" />
								<?php else :?>
								<img src="<?php echo get_template_directory_uri(); ?>/library/images/search-icon-white.svg" />
							<?php endif; ?>	
						</a>	
					</div>

				</div>
				
					<?php include (TEMPLATEPATH . '/includes/search-overlay.php' ); ?>
					<?php include (TEMPLATEPATH . '/includes/menu-overlay.php' ); ?>

			</header>
