<?php
/*
 Template Name: Home
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>

			<div id="content">

			<?php
				if ( get_query_var('paged') ) $paged = get_query_var('paged');  
				if ( get_query_var('page') ) $paged = get_query_var('page');
			 
			 	// this is pulling in 1 'per page', but we have three others loading in.  Currently done with recent_posts function.
				$query = new WP_Query( array( 'post_type' => 'post', 'posts_per_page' => 1, 'paged' => $paged ) );
		 
				if ( $query->have_posts() ) : ?>
					<?php while ( $query->have_posts() ) : $query->the_post(); ?>	
					<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>

					<div id="home-main">
						<?php if( have_rows('profile_homearchive_display') ): ?>
							
								<?php while( have_rows('profile_homearchive_display') ): the_row(); ?>

									<?php if( have_rows('full_screen_background') ): ?>
										<?php while( have_rows('full_screen_background') ): the_row(); ?>
											<?php 
												$backgroundtype = get_sub_field('background_type');
											?>
												<?php if( $backgroundtype === 'animated gif' ): ?>
													<?php $gif = get_sub_field('animated_gif'); ?>
														
														<div class="home-main-post" style="background-image: url('<?php echo $gif['url']; ?>')">
															<!-- 
																<div id="home-intro-video">
															        <iframe src="https://player.vimeo.com/video/92427777?autoplay=true&loop=1" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe>
																</div> 
															-->
															<div id="home-overlay"></div>
															<?php include (TEMPLATEPATH . '/includes/home-main-post-content.php' ); ?>

														</div>
												<?php endif; ?>

												<?php if( $backgroundtype === 'still image' ): ?>
													<?php $stillimage = get_sub_field('still_image'); ?>
														<div class="home-main-post" style="background-image: url('<?php echo $stillimage['url']; ?>')">
															<div id="home-overlay"></div>
															<?php include (TEMPLATEPATH . '/includes/home-main-post-content.php' ); ?>
														</div>
												<?php endif; ?>

												<?php if( $backgroundtype === 'vimeo' ): ?>
													<?php $video = get_sub_field('source'); ?>
														<div class="home-main-post">
															 
																<div id="home-intro-video">
															        <video muted id = "splash" width="100%" autoplay loop preload="auto" playsinline>
																		<source src="<?php echo $video; ?>" type = "video/mp4">
																	</video>
																</div> 
															
															<div id="home-overlay"></div>
															<?php include (TEMPLATEPATH . '/includes/home-main-post-content.php' ); ?>

														</div>
												<?php endif; ?>
										<?php endwhile;  ?>											
											
									<?php endif; ?>	
									<!-- end full screen background -->

								<?php endwhile;  ?>

						<?php endif; ?>
					</div>			

					<?php endwhile; wp_reset_postdata(); ?>
					<!-- show pagination here -->
					<?php else : ?>
					<!-- show 404 error here -->
				<?php endif; ?>	
				<!-- end of first if posts query	-->

				<div id="home-post-nav">
					<div class="home-post-nav-container">
						<ul>
							<!-- probably a more elegant way to get the three next posts, but works well -->
							<?php
							$query = new WP_Query( array( 'post_type' => 'post', 'posts_per_page' => 4, 'offset' => 1, 'paged' => $paged ) );
							if ( $query->have_posts() ) : ?>
								<?php while ( $query->have_posts() ) : $query->the_post(); 
									$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'bones-thumb-500' );
									$category = get_the_category(); 
									
									// Get the ID of a given category
								    $category_id = get_cat_ID( 'Category Name' );
								 
								    // Get the URL of this category
								    $category_link = get_category_link( $category_id );
								    $sidepost = get_sub_field('side_post_display');

								?>	
									<li>
										
											<div class="top-post-container">
												<div class="top-thumb-info-container">
													<div class="top-thumb-info">
														<div class="title-container">
																
																<a href="<?php echo the_permalink(); ?>">
																	<h5><?php the_title(); ?></h5>
																</a>
															
														</div>
														<div class="title-category-container">
															<?php echo '<a href="'.get_category_link($category[0]->cat_ID).'" class="icon '.$category[0]->slug.'"><i><span></span></i>'.$category[0]->name.'</a>'; ?>
														</div>
													
													</div>						
												</div>
												
												<?php if( have_rows('profile_homearchive_display') ): ?>
												
													<?php while( have_rows('profile_homearchive_display') ): the_row(); ?>
														<?php if( have_rows('side_post_display') ): ?>
															<?php while( have_rows('side_post_display') ): the_row(); 
																$sidepostthumb = get_sub_field('image_format');
																$gif = get_sub_field('animated_gif_file');
																$still = get_sub_field('image_file');
															?>	
															<div class="top-thumb">
																<a href="<?php echo the_permalink(); ?>">
																	<?php if ($sidepostthumb == 'animated gif') : ?>
																	<div class="gif-thumb" style="background: url('<?php echo $gif['url']; ?>');"></div>	
																	
																	<?php elseif ($sidepostthumb == 'still image') : ?>
																		<div class="gif-thumb" style="background: url('<?php echo $still['url']; ?>');"></div>
																	<?php endif; ?>

																</a>
															</div>
															<?php endwhile; ?>
														<?php endif; ?>

													<?php endwhile; ?>
												<?php endif; ?>

											</div>
										
									</li>
								<?php endwhile; ?>
							<?php endif; ?>
							<?php wp_reset_postdata(); ?>
						</ul>
					</div>
				</div>

				<div id="home-lower-content">
					<div class="row">
						<div class="col-xs-6">
							<div class="popular">
							    <h3>Popular</h3>
									<ul>
										<?php $popularPost = new WP_Query(array('posts_per_page'=>1, 'cat' => '-88', 'meta_key'=>'popular_posts', 'orderby'=>'meta_value_num', 'order'=>'DESC'));
										while ($popularPost->have_posts()) : $popularPost->the_post(); 
										?>
										<li>
											<a href="<?php echo the_permalink(); ?>">
												<div class="top-post-container">
													<div class="top-thumb">
														<?php the_post_thumbnail('bones-thumb-1000'); ?>
													</div>
													<div class="top-thumb-info">
														<h5><?php the_title(); ?></h5>
														<h3><?php the_time("M jS, Y"); ?></h3>
														<p><?php echo get_the_excerpt(); ?></p>
														<a href="<?php echo the_permalink(); ?>" title="<?php echo the_title(); ?>">
															<h6>View More ></h6>
														</a>
													</div>
												</div>
											</a>
										</li>
										<?php endwhile; wp_reset_postdata(); ?>
									</ul>
							</div>
						</div>
						<div class="col-xs-6">
							<div class="featured">
								<h3>Featured</h3>
								<ul>
									<?php $featurePost = new WP_Query(array('posts_per_page'=>1, 'category_name' => 'feature', 'order'=>'DESC'));
									while ($featurePost->have_posts()) : $featurePost->the_post(); 
									?>
									<li>
										<a href="<?php echo the_permalink(); ?>">
											<div class="top-post-container">
												<div class="top-thumb">
													<?php the_post_thumbnail('bones-thumb-1000'); ?>
												</div>
												<div class="top-thumb-info">
													<h5><?php the_title(); ?></h5>
													<h3><?php the_time("M jS, Y"); ?></h3>
													<p><?php echo get_the_excerpt(); ?></p>
													<a href="<?php echo the_permalink(); ?>" title="<?php echo the_title(); ?>">
														<h6>View More ></h6>
													</a>
												</div>
											</div>
										</a>
									</li>
									<?php endwhile; wp_reset_postdata(); ?>
								</ul>
							</div>
						</div>
					</div>
					<a href="/archive">
						<div class="view-more-btn">
							View More
						</div>
					</a>
				</div>
			</div>
			<!-- end #content -->
		<div class="footer-divider"></div>

<?php get_footer(); ?>
