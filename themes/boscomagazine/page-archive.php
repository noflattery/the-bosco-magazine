<?php 
/*
Template Name: Full Archive
*/

get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap cf">
					<div id="main-content">
						<main id="main" class="m-all cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
						<h1 class="page-title">Archive</h1>
							<?php

							    // set up our archive arguments
							    $archive_args = array(
							      'post_type' => 'post',    // get only posts
							      'posts_per_page'=> -1   // this will display all posts on one page
							    );

							    // new instance of WP_Query
							    $archive_query = new WP_Query( $archive_args );

							?>

							  <div class="loop-archive">
							  		<?php $counter = 1 ?>
							  		<div class="row is-table-row">
							
									    <?php while ( $archive_query->have_posts() ) : $archive_query->the_post(); // run the custom loop ?>
									    	<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'bones-thumb-800' );?>

									    	<!-- item -->
											<div class="col-xs-6 col-sm-2 col-lg-2">
										      <div <?php post_class(); // output a post article ?>>
										      	<a href="<?php echo the_permalink(); ?>">
										      		<div class="archive-thumb" style="width:auto; height:100%; overflow:hidden;">
								      				<?php if( have_rows('profile_homearchive_display') ): ?>
														<?php while( have_rows('profile_homearchive_display') ): the_row(); ?>
															<?php if( have_rows('side_post_display') ): ?>
																<?php 
																while( have_rows('side_post_display') ): the_row(); 
																$sidepostthumb = get_sub_field('image_format');
																$gif = get_sub_field('animated_gif_file');
																$still = get_sub_field('image_file');
																?>
																	<?php if( $sidepostthumb === 'animated gif' ): ?>
																			<div class="cube-container" style="background: url('<?php echo $gif['url'];?>');"></div>
																			<img src="<?php echo $gif['url'];?>" />
																		<?php elseif ($sidepostthumb == 'still image') : ?>
																			<img src="<?php echo $thumb['0']; ?>" />
																	<?php endif; ?>
																<?php endwhile; ?>
															<?php endif; ?>
														<?php endwhile; ?>
													<?php endif; ?>
										      		</div>
										      	</a>
										        <h4><a href="<?php echo the_permalink(); ?>"><?php the_title(); ?></a></h4>
										        <span><?php the_time("M jS, Y"); ?></span>
										      </div>
										    </div>

										<?php if ($counter % 6 == 0){ echo '</div><div class="row is-table-row">';} ?>      

									    <?php $counter++ ;
									    endwhile; // end the custom loop ?>	
									</div>
								</div>

							  </div>

						</main>
						<?php bones_page_navi(); ?>
					</div>
				</div>

			</div>

<?php get_footer(); ?>
