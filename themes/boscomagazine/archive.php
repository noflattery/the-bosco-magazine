<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap cf">
					<div id="main-content">
						<main id="main" class="m-all t-3of3 d-7of7 cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

							<div class="loop-archive">
							  	
									<?php
									the_archive_title( '<h1 class="page-title">', '</h1>' );
									the_archive_description( '<div class="taxonomy-description">', '</div>' );
									?>
									<div class="row">
										<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

											<div class="col-xs-12 col-sm-4 col-lg-2">
												<div <?php post_class(); // output a post article ?>>
													<section class="entry-content cf">
														<div class="archive-thumb" style="width:auto; height:100%; overflow:hidden;">
															<a href="<?php echo the_permalink(); ?>" title="<?php the_title(); ?>">
																<?php the_post_thumbnail( 'bones-thumb-800' ); ?>
															</a>
														</div>
														<h4><a href="<?php echo the_permalink(); ?>"><?php the_title(); ?></a></h4>
														<?php the_excerpt(); ?>
												        <span><?php the_time("M jS, Y"); ?></span>

													</section>
												</div>
											</div>


										<?php endwhile; ?>

												<?php bones_page_navi(); ?>

										<?php else : ?>

												<article id="post-not-found" class="hentry cf">
													<header class="article-header">
														<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
													</header>
													<section class="entry-content">
														<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
													</section>
													<footer class="article-footer">
															<p><?php _e( 'This is the error message in the archive.php template.', 'bonestheme' ); ?></p>
													</footer>
												</article>

										<?php endif; ?>
									</div>
							</div>
						</main>
					</div>
				</div>

			</div>

<?php get_footer(); ?>
