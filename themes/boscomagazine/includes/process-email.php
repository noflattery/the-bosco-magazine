<?php 
	$errors = array(); 
	$form_data = array(); 

	$email = $_POST['email'];
	$status = 'pending';
	if(!empty($_POST['status'])){
	    $status = $_POST['status'];
	}
	
	$url = 'https://api-staging.thebos.co/subscribe';

	$data = array("email" => $email);
	$data_string = json_encode($data);

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$url);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
	curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	    'Content-Type: multipart/form-data',
	    'Content-Length: ' . strlen($data_string))
	);

	$result=curl_exec ($ch);
	curl_close ($ch);

	echo $result;

	/* Validation  */
	
	if (empty($_POST['email'])) { 
	    $errors['email'] = 'Email cannot be blank';
	}
	if (!empty($errors)) { 
	    $form_data['success'] = false;
	    $form_data['errors']  = $errors;
	}
	else { 
	    $form_data['success'] = true;
	    $form_data['posted'] = 'You have successfully subscribed!';
	}

	// Return the data back to mailing-list-form.php
	echo json_encode($form_data);

?>


