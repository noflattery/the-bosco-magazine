<div class="background-color-container">

	<?php $content = get_sub_field('block_content'); ?>

	<?php if ($content) : ?>
		<div class="section-copy bio-copy">
			<?php echo $content; ?>
		</div>
	<?php endif; ?>

</div>



