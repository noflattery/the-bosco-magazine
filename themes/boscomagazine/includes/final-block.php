<div class="background-color-container">
	<div class="section-copy">
		<?php 
			$final_word_header = get_sub_field('final_word_header');
			$final_word = get_sub_field('final_word'); 
			$shout_outs_header = get_sub_field('shout_outs_header');
			$shout_outs = get_sub_field('shout_outs'); 
			$follow_header = get_sub_field('follow_header');
		?>



		<div class="follow">

			<?php if( have_rows('social_links') ): ?>
				<?php while( have_rows('social_links') ): the_row(); ?>
					<?php 
						$instagram = get_sub_field('instagram'); 
						$facebook = get_sub_field('facebook'); 
						$twitter = get_sub_field('twitter'); 
						$tumblr = get_sub_field('tumblr'); 
					?>
					<h2><?php echo $follow_header; ?></h2>
					<div class="final-word">
						<?php echo $final_word; ?>
					</div>
					<div class="social-links">
						<ul>
							<?php if ($instagram) : ?>
								<li>
									<a href="https://www.instagram.com/<?php echo $instagram; ?>" title="<?php the_title(); ?> on Instagram" target="_blank">
									<?php include (TEMPLATEPATH . '/includes/social/instagram-vector.php' ); ?>
									</a>
								</li>
							<?php endif; ?> 

							<?php if ($facebook) : ?>
								<li>
									<a href="https://www.facebook.com/<?php echo $facebook; ?>" title="<?php the_title(); ?> on Facebook" target="_blank">
									<?php include (TEMPLATEPATH . '/includes/social/facebook-vector.php' ); ?>
									</a>
								</li>
							<?php endif; ?> 

							<?php if ($twitter) : ?>
								<li>
									<a href="https://www.twitter.com/<?php echo $twitter; ?>" title="<?php the_title(); ?> on Twitter" target="_blank">
										<?php include (TEMPLATEPATH . '/includes/social/twitter-vector.php' ); ?>
									</a>
								</li>
							<?php endif; ?> 

							<?php if ($tumblr) : ?>
								<li>
									<a href="https://<?php echo $tumblr; ?>.tumblr.com" title="<?php the_title(); ?> on Tumblr" target="_blank">
										<?php include (TEMPLATEPATH . '/includes/social/tumblr-vector.php' ); ?>
									</a>
								</li>
							<?php endif; ?> 
						</ul>
						
					</div>
				<?php endwhile;  ?>
			<?php endif; ?>
		</div>

		<div class="more-interviews">

			<ul>
				<?php
				$postID = $post->ID;
				$query = new WP_Query( array( 'post_type' => 'post', 'posts_per_page' => 4, 'post__not_in' => array($postID), 'paged' => $paged ) );
				if ( $query->have_posts() ) : ?>
					<?php while ( $query->have_posts() ) : $query->the_post(); 
						$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'bones-thumb-500' );
						$category = get_the_category(); 
						
						// Get the ID of a given category
					    $category_id = get_cat_ID( 'Category Name' );
					 
					    // Get the URL of this category
					    $category_link = get_category_link( $category_id );
					    
					?>	
					<li class="col-xs-12 col-sm-6 col-md-3 single-related">
						<div class="bottom-thumb">
							<div class="top-post-container">
								<div class="top-thumb-info-container">
									<div class="top-thumb-info">
										<div class="title-container">
												
												<a href="<?php echo the_permalink(); ?>">
													<h5><?php the_title(); ?></h5>
												</a>
											
										</div>
										<div class="title-category-container">
											<?php echo '<a href="'.get_category_link($category[0]->cat_ID).'" class="icon '.$category[0]->slug.'"><i><span></span></i>'.$category[0]->name.'</a>'; ?>
										</div>
									
									</div>						
								</div>
								<div class="top-thumb">
									<a href="<?php echo the_permalink(); ?>">
										<img src="<?php echo $thumb['0'];?>" />
									</a>
								</div>

							</div>
						</div>
					</li>
					<?php endwhile; ?>
				<?php endif; ?>
				<?php wp_reset_postdata(); ?>
			</ul>
		</div>
	</div>
	<!--end section-copy -->
</div>