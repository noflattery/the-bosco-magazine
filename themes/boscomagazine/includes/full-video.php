<div class="full-video-container">
	<?php if( have_rows('background_video') ): ?>
		<?php while( have_rows('background_video') ): the_row(); ?>
			<?php 
				$source = get_sub_field('source'); 
				$externallink = get_sub_field('external_url');
				$vimeolink = get_sub_field('vimeo_code');
				$gif = get_sub_field('gif_upload');
			?>
			<?php if( $source === 'external link' ): ?>
				<video src="<?php echo $externallink; ?>" class="img-responsive main-page-movie" autoplay="" loop="" playsinline></video>
			<?php endif;  ?>

			<?php if( $source === 'vimeo' ): ?>
				<video muted src="<?php echo $vimeolink; ?>" class="img-responsive main-page-movie" autoplay="" loop="" playsinline></video>
			<?php endif;  ?>

			<?php if( $source === 'gif' ): ?>
				<a href="<?php echo $gif['url']; ?>" class="image-gallery">
					<img src="<?php echo $gif['url']; ?>" />
				</a>
			<?php endif;  ?>
		<?php endwhile;  ?>
	<?php endif;  ?>
</div>

<?php
	$content = get_sub_field('block_content');
?>

<?php if ($content) : ?>
	<div class="section-copy" style="background: <?php echo $bgcolor; ?>;">
		<?php echo $content; ?>
	</div>
<?php endif; ?>




