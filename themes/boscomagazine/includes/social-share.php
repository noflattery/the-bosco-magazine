<?php $shareimage = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); ?>
<ul class="soc">
	<li><a href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&amp;t=<?php the_title(); ?>" title="Share this post on Facebook!" onclick="window.open(this.href);" target="_blank" class="icon-10 facebook"><svg viewBox="0 0 512 512"><path d="M211.9 197.4h-36.7v59.9h36.7V433.1h70.5V256.5h49.2l5.2-59.1h-54.4c0 0 0-22.1 0-33.7 0-13.9 2.8-19.5 16.3-19.5 10.9 0 38.2 0 38.2 0V82.9c0 0-40.2 0-48.8 0 -52.5 0-76.1 23.1-76.1 67.3C211.9 188.8 211.9 197.4 211.9 197.4z"/></svg><!--[if lt IE 9]><em>Facebook</em><![endif]--></a></li>

	<li><a href="http://twitter.com/home?status=Reading: <?php the_permalink(); ?>" title="Share this post on Twitter!" target="_blank" class="icon-26 twitter" title="Twitter"><svg viewBox="0 0 512 512"><path d="M419.6 168.6c-11.7 5.2-24.2 8.7-37.4 10.2 13.4-8.1 23.8-20.8 28.6-36 -12.6 7.5-26.5 12.9-41.3 15.8 -11.9-12.6-28.8-20.6-47.5-20.6 -42 0-72.9 39.2-63.4 79.9 -54.1-2.7-102.1-28.6-134.2-68 -17 29.2-8.8 67.5 20.1 86.9 -10.7-0.3-20.7-3.3-29.5-8.1 -0.7 30.2 20.9 58.4 52.2 64.6 -9.2 2.5-19.2 3.1-29.4 1.1 8.3 25.9 32.3 44.7 60.8 45.2 -27.4 21.4-61.8 31-96.4 27 28.8 18.5 63 29.2 99.8 29.2 120.8 0 189.1-102.1 185-193.6C399.9 193.1 410.9 181.7 419.6 168.6z"/></svg><!--[if lt IE 9]><em>Twitter</em><![endif]--></a></li>

	<li><a href="http://www.tumblr.com/share/link?url=<?php the_permalink(); ?>" target="blank_" class="icon-25 tumblr" title="Tumblr"><svg viewBox="0 0 512 512"><path d="M210.8 80.3c-2.3 18.3-6.4 33.4-12.4 45.2 -6 11.9-13.9 22-23.9 30.5 -9.9 8.5-21.8 14.9-35.7 19.5v50.6h38.9v124.5c0 16.2 1.7 28.6 5.1 37.1 3.4 8.5 9.5 16.6 18.3 24.2 8.8 7.6 19.4 13.4 31.9 17.5 12.5 4.1 26.8 6.1 43 6.1 14.3 0 27.6-1.4 39.9-4.3 12.3-2.9 26-7.9 41.2-15v-55.9c-17.8 11.7-35.7 17.5-53.7 17.5 -10.1 0-19.1-2.4-27-7.1 -5.9-3.5-10-8.2-12.2-14 -2.2-5.8-3.3-19.1-3.3-39.7v-91.1H345.5v-55.8h-84.4v-90H210.8z"/></svg><!--[if lt IE 9]><em>Tumblr</em><![endif]--></a></li>

	<li><a href="http://pinterest.com/pin/create/button/?url=<?php echo urlencode(get_permalink($post->ID)); ?>&media=<?php echo $shareimage[0]; ?>&description=<?php the_title(); ?>" count-layout="vertical" class="icon-20 pinterest" title="Pinterest" target="_blank"><svg viewBox="0 0 512 512"><path d="M266.6 76.5c-100.2 0-150.7 71.8-150.7 131.7 0 36.3 13.7 68.5 43.2 80.6 4.8 2 9.2 0.1 10.6-5.3 1-3.7 3.3-13 4.3-16.9 1.4-5.3 0.9-7.1-3-11.8 -8.5-10-13.9-23-13.9-41.3 0-53.3 39.9-101 103.8-101 56.6 0 87.7 34.6 87.7 80.8 0 60.8-26.9 112.1-66.8 112.1 -22.1 0-38.6-18.2-33.3-40.6 6.3-26.7 18.6-55.5 18.6-74.8 0-17.3-9.3-31.7-28.4-31.7 -22.5 0-40.7 23.3-40.7 54.6 0 19.9 6.7 33.4 6.7 33.4s-23.1 97.8-27.1 114.9c-8.1 34.1-1.2 75.9-0.6 80.1 0.3 2.5 3.6 3.1 5 1.2 2.1-2.7 28.9-35.9 38.1-69 2.6-9.4 14.8-58 14.8-58 7.3 14 28.7 26.3 51.5 26.3 67.8 0 113.8-61.8 113.8-144.5C400.1 134.7 347.1 76.5 266.6 76.5z"/></svg><!--[if lt IE 9]><em>Pinterest</em><![endif]--></a></li>

	<li><a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&media="<?php echo $shareimage[0]; ?>"&description=<?php the_title(); ?>" target="_blank" class="icon-17 linkedin" title="LinkedIn"><svg viewBox="0 0 512 512"><path d="M186.4 142.4c0 19-15.3 34.5-34.2 34.5 -18.9 0-34.2-15.4-34.2-34.5 0-19 15.3-34.5 34.2-34.5C171.1 107.9 186.4 123.4 186.4 142.4zM181.4 201.3h-57.8V388.1h57.8V201.3zM273.8 201.3h-55.4V388.1h55.4c0 0 0-69.3 0-98 0-26.3 12.1-41.9 35.2-41.9 21.3 0 31.5 15 31.5 41.9 0 26.9 0 98 0 98h57.5c0 0 0-68.2 0-118.3 0-50-28.3-74.2-68-74.2 -39.6 0-56.3 30.9-56.3 30.9v-25.2H273.8z"/></svg><!--[if lt IE 9]><em>LinkedIn</em><![endif]--></a></li>

	<li><a href="mailto:?subject=<?php wp_title(); ?>&amp;body=Check out this site: <?php the_permalink(); ?>."
   title="Share by Email" class="icon-8 email" title="Email"><svg viewBox="0 0 512 512"><path d="M101.3 141.6v228.9h0.3 308.4 0.8V141.6H101.3zM375.7 167.8l-119.7 91.5 -119.6-91.5H375.7zM127.6 194.1l64.1 49.1 -64.1 64.1V194.1zM127.8 344.2l84.9-84.9 43.2 33.1 43-32.9 84.7 84.7L127.8 344.2 127.8 344.2zM384.4 307.8l-64.4-64.4 64.4-49.3V307.8z"/></svg><!--[if lt IE 9]><em>Email</em><![endif]--></a></li>
	
	<li class="sms-share">
		<a href="sms:&amp;body=<?php wp_title(''); ?> | <?php the_permalink(); ?>" title="Text Message" onclick="essb_window('sms:&amp;body=<?php the_title(); ?> | <?php the_permalink(); ?>','sms','1539219560'); return false;" target="_blank" rel="nofollow">
			SMS
		</a>
	</li>

	<li class="permalink-share">
		<a href="#" title="Permalink">
			<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				 width="20px" height="20px" viewBox="0 0 20 20" enable-background="new 0 0 20 20" xml:space="preserve">
			<path fill="#E8E8E8" d="M18.018,8.98l-3.583,3.581c-1.979,1.979-5.187,1.979-7.163,0c-0.312-0.311-0.556-0.66-0.77-1.021
				l1.665-1.664c0.079-0.081,0.176-0.125,0.27-0.18c0.115,0.393,0.316,0.765,0.625,1.074c0.987,0.988,2.595,0.987,3.583,0l3.581-3.582
				c0.989-0.988,0.989-2.595,0-3.582c-0.986-0.988-2.594-0.988-3.581,0l-1.273,1.275C10.337,4.479,9.223,4.372,8.139,4.53l2.715-2.714
				c1.977-1.978,5.184-1.978,7.163,0C19.995,3.794,19.995,7.001,18.018,8.98z M8.546,14.869l-1.274,1.275
				c-0.988,0.987-2.595,0.987-3.583,0c-0.988-0.987-0.988-2.595,0-3.583L7.271,8.98c0.988-0.989,2.594-0.989,3.583,0
				c0.307,0.308,0.508,0.68,0.625,1.073c0.093-0.054,0.19-0.099,0.27-0.177l1.665-1.666c-0.214-0.361-0.457-0.71-0.77-1.021
				c-1.979-1.978-5.187-1.978-7.165,0l-3.582,3.582c-1.979,1.979-1.979,5.186,0,7.165c1.979,1.977,5.186,1.977,7.164,0l2.714-2.715
				C10.693,15.381,9.578,15.271,8.546,14.869z"/>
			</svg>
		</a>
	</li>
</ul>

<div class="permalink-input" style="display: none;">
	<input type="text" name="permalink" value="<?php the_permalink(); ?>" />
</div>