<form action="<?php echo get_template_directory_uri(); ?>/includes/process-email.php" method="post" id="embedded-subscribe-form" name="boscoSubscribe" class="form-group form-inline">
	<input type="email" value="" name="email" class="form-control bosco-form navigation-footer-email-signup-input required email" placeholder="Email Address" id="email"> 
	<button type="submit" name="subscribe" id="embedded-subscribe" class="btn btn-default bosco-button-highlight navigation-footer-email-signup-button"></button>
	<div class="throw_error"></div>
	<div id="success"></div>
</form>

<script type="text/javascript">
	jQuery(document).ready(function($) {
	    $('#embedded-subscribe-form').on("submit", "form", function(event) { 
	    	event.preventDefault();
	        $('#email + .throw_error').empty(); 
	        $('#success').empty();

	        // Validate fields if required using jQuery

	        var boscoSubscribe = { //Fetch form data
	            'email'     : $('input[email=email]').val()
	        };

	        $.ajax({
	            type      : 'POST', 
	            url       : '<?php echo get_template_directory_uri(); ?>/includes/process-email.php', 
	            data      : boscoSubscribe, 
	            dataType  : 'multipart/form-data',
	            error       : function(err) { alert("Please enter valid email."); },
	            success   : function(data) {
	            if (!data.success) { 
	                if (data.errors.data) { 
	                    $('.throw_error').fadeIn(500).html(data.errors.data); 
	                }
	            }
	            else {
	                    $('#success').fadeIn(500).append('<p>' + data.posted + '</p>');
	                }
	            }
	        });
	
	    });
	});
</script>