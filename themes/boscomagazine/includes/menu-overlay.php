<div class="overlay">
	<div class="">
		<div class="col-xs-4 col-md-4 header-logo-menu">
			<a href="<?php echo home_url(); ?>" rel="nofollow"><img src="<?php echo get_template_directory_uri(); ?>/library/images/boscomag-vector-black.svg" class="company-logo-menu" /></a>
			<?php // if you'd like to use the site description you can un-comment it below ?>
			<?php // bloginfo('description'); ?>
		</div>

		<div class="col-xs-4 col-md-4 menu-toggle-menu">
			<a class="btn-close" href="#">
				<img src="<?php echo get_template_directory_uri(); ?>/library/images/close-black.svg" />
			</a>
		</div>

		<div class="col-xs-4 col-md-4 search-button-menu">
			<img src="<?php echo get_template_directory_uri(); ?>/library/images/search-icon-black.svg" />				
		</div>
	</div>

	<div class="row overlay-menus">
		<div class="col-xs-12 col-sm-6 overlay-left">
			<div class="overlay-left-menu">
				<nav role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
					<?php wp_nav_menu(array(
				         'container' => false,                           // remove nav container
				         'container_class' => 'menu cf',                 // class of container (should you choose to use it)
				         'menu' => __( 'The Main Menu', 'bonestheme' ),  // nav name
				         'menu_class' => 'nav top-nav cf',               // adding custom nav class
				         'theme_location' => 'main-nav',                 // where it's located in the theme
				         'before' => '',                                 // before the menu
			               'after' => '',                                  // after the menu
			               'link_before' => '',                            // before each link
			               'link_after' => '',                             // after each link
			               'depth' => 0,                                   // limit the depth of the nav
				         'fallback_cb' => ''                             // fallback function (if there is one)
					)); ?>
				</nav>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 overlay-right">
			<div class="overlay-right-menu">
				<h5 class="first-right-header">About</h5>
				<p>The Bosco Magazine showcases up and coming and established artists from around the world. One artist is featured every week with the hope that they will inspire you as much as they do us.</p>

				<div class="overlay-contact">
					
						<h5>Contact</h5>
						<ul class="nav">
							<li class="email-address">
								<a href="mailto:Magazine@thebosco.com">Magazine@thebosco.com</a>
							</li>
							<li>
								<div class="social-links">
									<ul>					
										<li>
											<a href="https://www.instagram.com/thebosco/" title="Instagram" target="_blank">
											<?php include (TEMPLATEPATH . '/includes/social/instagram-vector.php' ); ?>
											</a>
										</li>
								
										<li>
											<a href="https://www.facebook.com/theboscobooth" title="Facebook" target="_blank">
											<?php include (TEMPLATEPATH . '/includes/social/facebook-vector.php' ); ?>
											</a>
										</li>

										<li>
											<a href="https://twitter.com/thebosco" title="Twitter" target="_blank">
												<?php include (TEMPLATEPATH . '/includes/social/twitter-vector.php' ); ?>
											</a>
										</li>

										<li>
											<a href="http://thebosco.tumblr.com/" title="Tumblr" target="_blank">
												<?php include (TEMPLATEPATH . '/includes/social/tumblr-vector.php' ); ?>
											</a>
										</li>
									</ul>
								</div>
							</li>

						</ul>
				</div>

				<div class="overlay-fam">
					<h5>The Fam</h5>
					<ul>
						<li class="menu-item"><a href="http://thebosco.com"><?php include (TEMPLATEPATH . '/includes/bosco-vector.php' ); ?></a></li>
						<li class="menu-item"><a href="http://curfew.tv"><?php include (TEMPLATEPATH . '/includes/curfew-vector.php' ); ?></a></li>
						<li class="menu-item"><a href="http://bobbyredd.com"><?php include (TEMPLATEPATH . '/includes/bobby-redd-vector.php' ); ?></a></li>
					</ul>
				</div>

				
			</div>
		</div>
	</div>
</div>