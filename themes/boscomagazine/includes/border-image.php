<div class="border-image-container">

	<?php 
		$image = get_sub_field('first_image'); 
		$content = get_sub_field('block_content');
	?>

	<div class="border-image">
		<a href="<?php echo $image['url']; ?>" class="image-gallery">
			<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		</a>
	</div>
</div>

<?php if ($content) : ?>
	<div class="section-copy">
		<?php echo $content; ?>
	</div>
<?php endif; ?>


