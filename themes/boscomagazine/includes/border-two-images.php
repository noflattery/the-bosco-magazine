<div class="border-image-container">

	<?php 
		$image1 = get_sub_field('first_image'); 
		$image2 = get_sub_field('second_image');
		$content = get_sub_field('block_content');
	?>

	<div class="row">
		<div class="col-sm-6">
			<div class="border-image image-spacing">
				<a href="<?php echo $image1['url']; ?>" class="image-gallery">
					<img src="<?php echo $image1['url']; ?>" alt="<?php echo $image1['alt']; ?>" />
				</a>
			</div>
		</div>

		<div class="col-sm-6">
			<div class="border-image image-spacing">
				<a href="<?php echo $image2['url']; ?>" class="image-gallery">
					<img src="<?php echo $image2['url']; ?>" alt="<?php echo $image2['alt']; ?>" />
				</a>
			</div>
		</div>
	</div>

</div>

<?php if ($content) : ?>
	<div class="section-copy">
		<?php echo $content; ?>
	</div>
<?php endif; ?>


