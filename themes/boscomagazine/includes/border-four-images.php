<div class="border-image-container">

	<?php 
		$image1 = get_sub_field('first_image'); 
		$image2 = get_sub_field('second_image'); 		
		$image3 = get_sub_field('third_image'); 
		$image4 = get_sub_field('fourth_image'); 
		$content = get_sub_field('block_content');
	?>

	<div class="row">
		<div class="col-sm-6 grid-4-col">
			<div class="border-image image-spacing">
				<a href="<?php echo $image1['url']; ?>" class="image-gallery">	
					<img src="<?php echo $image1['url']; ?>" alt="<?php echo $image1['alt']; ?>" />
				</a>
			</div>
		</div>

		<div class="col-sm-6 grid-4-col">
			<div class="border-image image-spacing">
				<a href="<?php echo $image2['url']; ?>" class="image-gallery">
					<img src="<?php echo $image2['url']; ?>" alt="<?php echo $image2['alt']; ?>" />
				</a>
			</div>
		</div>

	</div>

	<div class="row">
		<div class="col-sm-6 grid-4-col">
			<div class="border-image image-spacing">
				<a href="<?php echo $image3['url']; ?>" class="image-gallery">
					<img src="<?php echo $image3['url']; ?>" alt="<?php echo $image3['alt']; ?>" />
				</a>
			</div>
		</div>

		<div class="col-sm-6 grid-4-col">
			<div class="border-image image-spacing">
				<a href="<?php echo $image4['url']; ?>" class="image-gallery">
					<img src="<?php echo $image4['url']; ?>" alt="<?php echo $image4['alt']; ?>" />
				</a>
			</div>
		</div>
	</div>

</div>

<?php if ($content) : ?>
	<div class="section-copy">
		<?php echo $content; ?>
	</div>
<?php endif; ?>



