<?php $bgimage = get_sub_field('full_screen_image'); ?>

<div class="full-image-container" style="opacity: 0.5; background-image: url('<?php echo $bgimage['url']; ?>')"></div>