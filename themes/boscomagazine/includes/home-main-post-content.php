<div class="home-main-post-content">
															
	<a href="<?php the_permalink(); ?>" title="<?php echo the_title(); ?>">
		<h2 class="title"><?php the_title(); ?></h2>
		<?php
			// if ( has_post_thumbnail()) {
			// 	echo '<a href="' . get_permalink($post->ID) . '" >';
			// 	the_post_thumbnail( $post_id, $size, $attr );
			// 	echo '</a>';
			// }
		?>
		
	</a>

	<?php 
		$featured_gif = get_field('featured_gif');
		if( !empty($featured_gif) ): ?>
				<img src="<?php echo $featured_gif['url']; ?>" alt="<?php echo $featured_gif['alt']; ?>" class="projects-image" />
		<?php endif; 
	?>
	
	<?php
	    // Get the ID of a given category
	    $category_id = get_cat_ID( 'Category Name' );
	 
	    // Get the URL of this category
	    $category_link = get_category_link( $category_id );
	?>	
	<h3><?php the_time("M jS, Y"); ?></h3>
	<p><?php echo get_the_excerpt(); ?></p>		
	<a href="<?php the_permalink(); ?>"><h4>View More ></h4></a>				
	
</div>