<div class="full-video-container" style="height: 100vh; width: 100%;">
<?php if( have_rows('background_video') ): ?>
	<?php while( have_rows('background_video') ): the_row(); ?>
		<?php 
			$source = get_sub_field('source'); 
		?>
		<?php if( $source === 'external link' ): $externallink = get_sub_field('external_url'); ?>
			<video muted id = "splash" width="100%" autoplay loop preload="auto" playsinline>
				<source src="<?php echo $externallink; ?>" type = "video/mp4">
			</video>
		<?php endif;  ?>

		<?php if( $source === 'vimeo' ): $video_id = get_sub_field('video_id'); ?>
			<video muted id = "splash" width="100%" autoplay loop preload="auto" style="opacity: 0.5" playsinline>
				<source src="<?php echo $video_id; ?>" type = "video/mp4">
			</video>
		<?php endif;  ?>

		<?php if( $source === 'gif' ): $gif_upload = get_sub_field('gif_upload'); ?>
			<div class="profile-index-gif" style="background-image: url('<?php echo $gif_upload['url']; ?>')">
			</div>
		<?php endif;  ?>

	<?php endwhile;  ?>
<?php endif;  ?>
</div>