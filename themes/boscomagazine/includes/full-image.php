<div class="full-image-container">

	<?php 
		$image = get_sub_field('background_image'); 
		$content = get_sub_field('block_content');
	?>

	<div class="full-image">
		<a href="<?php echo $image['url']; ?>" class="image-gallery">
			<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		</a>
	</div>
</div>

<?php if ($content) : ?>
	<div class="section-copy" style="background: <?php echo $bgcolor; ?>;">
		<?php echo $content; ?>
	</div>
<?php endif; ?>



