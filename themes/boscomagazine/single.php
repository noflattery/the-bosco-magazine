<?php get_header(); ?>

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<div class="profile-index">
					
						<?php if( have_rows('profile_index') ): ?>
						
							<?php 
								while( have_rows('profile_index') ): the_row(); 
								$format = get_sub_field('block_format');
							?>
								
									<?php if( $format === 'background color' ): ?>
										<?php $bgcolor = get_sub_field('background_color'); ?>

										<div id="profile-intro-background" style="background: <?php echo $bgcolor; ?>">
											<section class="profile-block">
												<?php include (TEMPLATEPATH . '/includes/full-bgcolor-index.php' ); ?>
											</section>
										</div>
									<?php endif; ?>

									<?php if( $format === 'full screen video' ): ?>
										<div id="profile-intro-background">
											<section class="profile-block">
												<?php include (TEMPLATEPATH . '/includes/full-bgvideo-index.php' ); ?>
											</section>
										</div>
									<?php endif; ?>

									<?php if( $format === 'full screen image' ): ?>
										<div id="profile-intro-background">
											<section class="profile-block">
												<?php include (TEMPLATEPATH . '/includes/full-bgimage-index.php' ); ?>
											</section>
										</div>
									<?php endif; ?>
							<?php endwhile;  ?>

					<?php endif; ?>
					
					<section class="profile-intro">
						<h1><?php echo the_title(); ?></h1>
						<h3><?php the_time("M jS, Y"); ?></h3>
						<div class="share-button-intro">
							<img src="<?php echo get_template_directory_uri(); ?>/library/images/share-icon.svg" />
						</div>
						<div class="share-icons-intro">
							<div class="social-share">
	                        	<?php include (TEMPLATEPATH . '/includes/social-share.php' ); ?>
                   			</div>
						</div>
					</section>
				</div>
				
				<div id="fixed-profile-title">
					<h2><?php echo the_title(); ?></h2>
				</div>

				<div id="fixed-nav-arrow">
					<img src="<?php echo get_template_directory_uri(); ?>/library/images/down-arrow.svg" alt="down" />
				</div>

					<?php if( have_rows('profile_block') ): ?>
						
							<?php 
								while( have_rows('profile_block') ): the_row(); 
								$format = get_sub_field('block_format');
								$bgcolor = get_sub_field('background_color');
							?>
								
								<?php if( $format === 'full screen video' ): ?>
									<section class="profile-block">
										<?php include (TEMPLATEPATH . '/includes/full-video.php' ); ?>
									</section>
								<?php endif; ?>

								<?php if( $format === 'full screen image' ): ?>
									<section class="profile-block">
										<?php include (TEMPLATEPATH . '/includes/full-image.php' ); ?>
									</section>
								<?php endif; ?>

								<?php if( $format === 'single image' ): ?>
									<section class="profile-block" style="background: <?php echo $bgcolor; ?>;">
										<?php include (TEMPLATEPATH . '/includes/border-image.php' ); ?>
									</section>
								<?php endif; ?>


								<?php if( $format === 'two images' ): ?>
									<section class="profile-block" style="background: <?php echo $bgcolor; ?>;">
										<?php include (TEMPLATEPATH . '/includes/border-two-images.php' ); ?>
									</section>
								<?php endif; ?>

								<?php if( $format === 'four images' ): ?>
									<section class="profile-block" style="background: <?php echo $bgcolor; ?>;">
										<?php include (TEMPLATEPATH . '/includes/border-four-images.php' ); ?>
									</section>
								<?php endif; ?>

								<?php if( $format === 'background color' ): ?>
									<?php $bgcolor = get_sub_field('background_color'); ?>
									<section class="profile-block" style="background: <?php echo $bgcolor; ?>;">
										<?php include (TEMPLATEPATH . '/includes/background-color.php' ); ?>
									</section>
								<?php endif; ?>

								<?php if( $format === 'bio solid color' ): ?>
									<section class="profile-block" style="background: <?php echo $bgcolor; ?>;">
										<?php include (TEMPLATEPATH . '/includes/bio-solid-color.php' ); ?>
									</section>
								<?php endif; ?>

							<?php endwhile;  ?>

					<?php endif; ?>

					<?php if( have_rows('profile_final_block') ): ?>
						
							<?php while( have_rows('profile_final_block') ): the_row(); ?>

								<section class="profile-block final-block">
									<?php include (TEMPLATEPATH . '/includes/final-block.php' ); ?>
								</section>

							<?php endwhile;  ?>

					<?php endif; ?>
				
		<?php endwhile; ?>

		<?php else : ?>

			<article id="post-not-found" class="hentry cf">
					<header class="article-header">
						<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
					</header>
					<section class="entry-content">
						<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
					</section>
					<footer class="article-footer">
							<p><?php _e( 'This is the error message in the single.php template.', 'bonestheme' ); ?></p>
					</footer>
			</article>

		<?php endif; ?>


		<script type="text/javascript">
			jQuery(document).ready(function($) {
				$('.image-gallery').featherlightGallery({
					previousIcon: '<',     /* Code that is used as previous icon */
					nextIcon: '>',         /* Code that is used as next icon */
				});

				$('.slideshow-button').on('click', function(){
					$('.image-gallery').eq(0).trigger('click'); 
				});
			});
		</script>



<?php get_footer(); ?>
