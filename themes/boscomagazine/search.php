<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap cf">
					<div id="main-content">
						<main id="main" class="m-all cf" role="main">
							<h1 class="archive-title"><span><?php _e( 'Search Results for:', 'bonestheme' ); ?></span> <?php echo esc_attr(get_search_query()); ?></h1>

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

								<div class="loop-archive">
							  		<div class="row">
							  			<div class="col-xs-6 col-sm-6 col-lg-2">
											<?php 
												$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'bones-thumb-400' );
												$date_new = get_the_time("F Y"); // get $date_new in "Month Year" format
											?>
											<div <?php post_class(); // output a post article ?>>
												<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
													<img src="<?php echo $thumb['0']; ?>" />
												</a>
					
													<h4 class="search-title entry-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>

				                  						<p class="byline entry-meta vcard">
				                    							<?php printf( __( 'Posted %1$s by %2$s', 'bonestheme' ),
				                   							    /* the time the post was published */
				                   							    '<time class="updated entry-time" datetime="' . get_the_time('F Y') . '" itemprop="datePublished">' . get_the_time(get_option('date_format')) . '</time>'
				                      							    /* the author of the post */
				                    							); ?>
				                    							<span><?php the_time("M jS, Y"); ?></span>
				                  						</p>

												<section class="entry-content">
														<?php the_excerpt( '<span class="read-more">' . __( 'Read more &raquo;', 'bonestheme' ) . '</span>' ); ?>
												</section>

													<?php if(get_the_category_list(', ') != ''): ?>
				                  					<?php printf( __( '<span>Categories:</span> %1$s', 'bonestheme' ), get_the_category_list(', ') ); ?>
				                  					<?php endif; ?>

				                 					<?php the_tags( '<p class="tags"><span class="tags-title">' . __( 'Tags:', 'bonestheme' ) . '</span> ', ', ', '</p>' ); ?>

											</div>
										</div>
									</div>
								</div>

							<?php endwhile; ?>

									<?php bones_page_navi(); ?>

								<?php else : ?>

										<article id="post-not-found" class="hentry cf">
											<header class="article-header">
												<h1><?php _e( 'Sorry, No Results.', 'bonestheme' ); ?></h1>
											</header>
											<section class="entry-content">
												<p><?php _e( 'Try your search again.', 'bonestheme' ); ?></p>
											</section>
											<footer class="article-footer">
													<p><?php _e( 'This is the error message in the search.php template.', 'bonestheme' ); ?></p>
											</footer>
										</article>

								<?php endif; ?>

							</main>
						</div>
					</div>

			</div>

<?php get_footer(); ?>