			<?php if (is_front_page()) : ?>
				<footer class="footer" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">

					<div id="inner-footer" class="wrap cf">
						<div class="row">
							<div class="col-xs-12 col-md-6">
								<div class="row">
									<div class="col-xs-6 col-sm-6">
										<div class="footer-menu">
											<h5>Explore</h5>
											<h6>-</h6>
											<nav role="navigation">
												<?php wp_nav_menu(array(
						    					'container' => 'div',                           // enter '' to remove nav container (just make sure .footer-links in _base.scss isn't wrapping)
						    					'container_class' => 'footer-links cf',         // class of container (should you choose to use it)
						    					'menu' => __( 'Footer Menu One', 'bonestheme' ),   // nav name
						    					'menu_class' => 'nav footer-nav cf',            // adding custom nav class
						    					'theme_location' => 'footer-menu-one',             // where it's located in the theme
						    					'before' => '',                                 // before the menu
						    					'after' => '',                                  // after the menu
						    					'link_before' => '',                            // before each link
						    					'link_after' => '',                             // after each link
						    					'depth' => 0,                                   // limit the depth of the nav
						    					'fallback_cb' => 'bones_footer_links_fallback'  // fallback function
												)); ?>
											</nav>
										</div>
									</div>
									<div class="col-xs-6 col-sm-6">
										<div class="footer-menu">
											<h5>The Fam</h5>
											<h6>-</h6>
											<nav role="navigation">
												<div class="footer-links cf">
													<ul id="menu-the-fam" class="nav footer-nav cf">
														<li class="menu-item"><a href="http://thebosco.com" target="_blank"><?php include (TEMPLATEPATH . '/includes/bosco-vector.php' ); ?></a></li>
														<li class="menu-item"><a href="http://curfew.tv" target="_blank"><?php include (TEMPLATEPATH . '/includes/curfew-vector.php' ); ?></a></li>
														<li class="menu-item"><a href="http://bobbyredd.com" target="_blank"><?php include (TEMPLATEPATH . '/includes/bobby-redd-vector.php' ); ?></a></li>
													</ul>
												</div>
											</nav>
										</div>
									</div>
								</div>
							</div>
						

							<div class="col-xs-12 col-md-6">
								<div class="mailing-list">
									<h5>To Stay Inspired</h5>
									<h3>All The Time</h3>
									<?php include (TEMPLATEPATH . '/includes/mailing-list-form.php' ); ?>
								</div>
								<div class="mail-social">

									<h6><a href="mailto:magazine@thebosco.com" title="E-mail The Bosco" />magazine@thebosco.com</a></h6>
									<div class="social-links">
										<ul>					
											<li>
												<a href="https://www.instagram.com/thebosco/" title="Instagram" target="_blank">
												<?php include (TEMPLATEPATH . '/includes/social/instagram-vector.php' ); ?>
												</a>
											</li>
									
											<li>
												<a href="https://www.facebook.com/theboscobooth" title="Facebook" target="_blank">
												<?php include (TEMPLATEPATH . '/includes/social/facebook-vector.php' ); ?>
												</a>
											</li>

											<li>
												<a href="https://twitter.com/thebosco" title="Twitter" target="_blank">
													<?php include (TEMPLATEPATH . '/includes/social/twitter-vector.php' ); ?>
												</a>
											</li>

											<li>
												<a href="http://thebosco.tumblr.com/" title="Tumblr" target="_blank">
													<?php include (TEMPLATEPATH . '/includes/social/tumblr-vector.php' ); ?>
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>

				</footer>
			<?php else: ?>
				<footer id="fixed-footer" class="footer" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">
					<div class="footer-banner">
						<div id="dots-open">
							<div class="dots-container">
								<div class="dot-single"></div>
								<div class="dot-single"></div>
								<div class="dot-single"></div>								
							</div>
						</div>
				
						<div class="slideshow-button">
							<img src="<?php echo get_template_directory_uri(); ?>/library/images/slideshow-btn.svg" />
						</div>
						<div class="share-button">
							<img src="<?php echo get_template_directory_uri(); ?>/library/images/share-icon.svg" />
						</div>

						<div class="share-icons">
							<div class="social-share">
	                        	<?php include (TEMPLATEPATH . '/includes/social-share.php' ); ?>
                   			</div>
						</div>
					</div>
					<div id="inner-footer" class="wrap cf">
						<div class="row">
							<div class="col-xs-12 col-md-6">
								<div class="row">
									<div class="col-xs-6 col-sm-6">
										<div class="footer-menu">
											<h5>Explore</h5>
											<h6>-</h6>
											<nav role="navigation">
												<?php wp_nav_menu(array(
						    					'container' => 'div',                           // enter '' to remove nav container (just make sure .footer-links in _base.scss isn't wrapping)
						    					'container_class' => 'footer-links cf',         // class of container (should you choose to use it)
						    					'menu' => __( 'Footer Menu One', 'bonestheme' ),   // nav name
						    					'menu_class' => 'nav footer-nav cf',            // adding custom nav class
						    					'theme_location' => 'footer-menu-one',             // where it's located in the theme
						    					'before' => '',                                 // before the menu
						    					'after' => '',                                  // after the menu
						    					'link_before' => '',                            // before each link
						    					'link_after' => '',                             // after each link
						    					'depth' => 0,                                   // limit the depth of the nav
						    					'fallback_cb' => 'bones_footer_links_fallback'  // fallback function
												)); ?>
											</nav>
										</div>
									</div>
									<div class="col-xs-6 col-sm-6">
										<div class="footer-menu">
											<h5>The Fam</h5>
											<h6>-</h6>
											<ul>
												<li class="menu-item fam-li"><a href="http://thebosco.com"><?php include (TEMPLATEPATH . '/includes/bosco-vector.php' ); ?></a></li>
												<li class="menu-item fam-li"><a href="http://curfew.tv"><?php include (TEMPLATEPATH . '/includes/curfew-vector.php' ); ?></a></li>
												<li class="menu-item fam-li"><a href="http://bobbyredd.com"><?php include (TEMPLATEPATH . '/includes/bobby-redd-vector.php' ); ?></a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						

							<div class="col-xs-12 col-md-6">
								<div class="mailing-list">
									<h5>To Stay Inspired</h5>
									<h3>All The Time</h3>
									<?php include (TEMPLATEPATH . '/includes/mailing-list-form.php' ); ?>
								</div>
								<div class="social-links">
									<ul>					
										<li>
											<a href="https://www.instagram.com/thebosco/" title="Instagram" target="_blank">
											<?php include (TEMPLATEPATH . '/includes/social/instagram-vector.php' ); ?>
											</a>
										</li>
								
										<li>
											<a href="https://www.facebook.com/theboscobooth" title="Facebook" target="_blank">
											<?php include (TEMPLATEPATH . '/includes/social/facebook-vector.php' ); ?>
											</a>
										</li>

										<li>
											<a href="https://twitter.com/thebosco" title="Twitter" target="_blank">
												<?php include (TEMPLATEPATH . '/includes/social/twitter-vector.php' ); ?>
											</a>
										</li>

										<li>
											<a href="http://thebosco.tumblr.com/" title="Tumblr" target="_blank">
												<?php include (TEMPLATEPATH . '/includes/social/tumblr-vector.php' ); ?>
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>

				</footer>
			<?php endif; ?>

		<?php // all js scripts are loaded in library/bones.php ?>
		<?php wp_footer(); ?>

	</body>

</html> <!-- end of site. what a ride! -->
