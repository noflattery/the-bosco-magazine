<?php 
/*Plugin Name: Custom Post Types for Bosco Magazine Profiles
Description: 
Version: 1.0
License: GPLv2
*/

function photographer() {
    // set up labels
    $labels = array(
        'name' => 'Photographers',
        'singular_name' => 'Photography',
        'add_new' => 'Add New Photographer Post',
        'add_new_item' => 'Add New Photographer Post',
        'edit_item' => 'Edit Photographer Post',
        'new_item' => 'New Photographer Post',
        'all_items' => 'All Photographer Posts',
        'view_item' => 'View Photographer Post',
        'search_items' => 'Search Photographer Posts',
        'not_found' =>  'No Photographer Post Found',
        'not_found_in_trash' => 'No Photographer Post found in Trash', 
        'parent_item_colon' => '',
        'menu_name' => 'Photographers',
        'menu_position' => 1,
        'show_ui' => true,
        'query_var' => true,
    );
    //register post type
    register_post_type( 'photographer', array(
        'labels' => $labels,
        'has_archive' => true,
        'public' => true,
        'supports' => array( 'title', 'editor', 'excerpt', 'custom-fields', 'thumbnail', 'page-attributes' ),
        'taxonomies' => array( 'post_tag', 'category' ),    
        'exclude_from_search' => false,
        'capability_type' => 'post',
        'rewrite' => array( 'slug' => 'photography' ),
        )
    );
}
add_action( 'init', 'photographer' );


function digital_artist() {
    // set up labels
    $labels = array(
        'name' => 'Digital Artists',
        'singular_name' => 'Digital Artist',
        'add_new' => 'Add New Digital Artist Post',
        'add_new_item' => 'Add New Digital Artist Post',
        'edit_item' => 'Edit Digital Artist Post',
        'new_item' => 'New Digital Artist Post',
        'all_items' => 'All Digital Artist Posts',
        'view_item' => 'View Digital Artist Post',
        'search_items' => 'Search Digital Artist Posts',
        'not_found' =>  'No Digital Artist Post Found',
        'not_found_in_trash' => 'No Digital Artist Post found in Trash', 
        'parent_item_colon' => '',
        'menu_name' => 'Digital Artists',
        'menu_position' => 1,
        'show_ui' => true,
        'query_var' => true,
    );
    //register post type
    register_post_type( 'digitalartist', array(
        'labels' => $labels,
        'has_archive' => true,
        'public' => true,
        'supports' => array( 'title', 'editor', 'excerpt', 'custom-fields', 'thumbnail', 'page-attributes' ),
        'taxonomies' => array( 'post_tag', 'category' ),    
        'exclude_from_search' => false,
        'capability_type' => 'post',
        'rewrite' => array( 'slug' => 'digitalartists' ),
        )
    );
}
add_action( 'init', 'digital_artist' );

function animator() {
    // set up labels
    $labels = array(
        'name' => 'Animators',
        'singular_name' => 'Animator',
        'add_new' => 'Add New Animator Post',
        'add_new_item' => 'Add New Animator Post',
        'edit_item' => 'Edit Animator Post',
        'new_item' => 'New Animator Post',
        'all_items' => 'All Animator Posts',
        'view_item' => 'View Animator Post',
        'search_items' => 'Search Animator Posts',
        'not_found' =>  'No Animator Post Found',
        'not_found_in_trash' => 'No Animator Post found in Trash', 
        'parent_item_colon' => '',
        'menu_name' => 'Animators',
        'menu_position' => 1,
        'show_ui' => true,
        'query_var' => true,
    );
    //register post type
    register_post_type( 'animator', array(
        'labels' => $labels,
        'has_archive' => true,
        'public' => true,
        'supports' => array( 'title', 'editor', 'excerpt', 'custom-fields', 'thumbnail', 'page-attributes' ),
        'taxonomies' => array( 'post_tag', 'category' ),    
        'exclude_from_search' => false,
        'capability_type' => 'post',
        'rewrite' => array( 'slug' => 'animators' ),
        )
    );
}
add_action( 'init', 'animator' );
?>