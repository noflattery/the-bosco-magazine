<?php
/*
Plugin Name: Show Template
Plugin URI: http://tim-beckett.com
Description: Generic Code to Show Template files at top of page for easy location and editing. Placed in plugin folder to prevent accidental duplication to live site when theme updates are uploaded. 
Version: 4.1.1
Author: Tim Beckett
Author URI: http://tim-beckett.com

/***********
 =SHOW TEMPLATE
 **************/
add_action('wp_head', 'show_template');
function show_template() {  
    global $template;
    echo '<span style="color: #fff;">' . $template . '</span>';
    echo '<span style="color: #000;">' . $template . '</span>';
}